module gitlab.com/akita/mem

require (
	github.com/golang/mock v1.4.3
	github.com/google/btree v1.0.0
	github.com/onsi/ginkgo v1.12.0
	github.com/onsi/gomega v1.9.0
	github.com/rs/xid v1.2.1
	gitlab.com/akita/akita v1.10.1
	gitlab.com/akita/util v0.4.0
	go.mongodb.org/mongo-driver v1.3.1 // indirect
	google.golang.org/appengine v1.6.5 // indirect
)

// replace gitlab.com/akita/akita => ../akita

// replace gitlab.com/akita/util => ../util

go 1.13
